const express = require('express');
const bodyParser = require('body-parser');
const { v4: uuidv4 } = require('uuid');

const redis = require('redis');
// Create Redis Clients
const client = redis.createClient({ url: 'redis://127.0.0.1:6379' });
let publisher = client.duplicate();
let subscriber = client.duplicate();
let keyExpiredHandle = client.duplicate();
// Connect all clients - init
(async () => {
  await client.connect();
  await publisher.connect();
  await subscriber.connect();
  await keyExpiredHandle.connect();
})();
// if (!client.isReady) {
//   await client.connect()
// }
// Even got clone of client - they just using on handle (p)subscribe/(p)unsubscribe/quit

// Client Auto update when restart services
async function getAllKeys(pattern = "*") {
  if (await client.isOpen === false) { await client.connect(); }

  const keys = [];
  const iteratorParams = {
    MATCH: pattern
  }
  for await (const key of client.scanIterator(iteratorParams)) {
    keys.push(key);
  }

  return { keys };
}

async function updatePreviousKey() {
  const { keys } = await getAllKeys("*");
  const needUpdate = [];
  if (await client.isOpen === false) { await client.connect(); }

  const promises = keys.map(async key => {
    let head = key.slice(0, -2);
    let tail = key.slice(-2);

    if (await client.ttl(key) == -1) {
      await client.del(key);
    } else if (!needUpdate.includes(key) && tail === "-l") {
      needUpdate.push(key);
    } else if (tail === "-s") {
      const tmp = head + "-l";
      const tmpIndex = needUpdate.indexOf(tmp);
      if (tmpIndex !== -1) {
        needUpdate.splice(tmpIndex, 1);
      }
    }
  });

  await Promise.all(promises);

  needUpdate.forEach(async key => {
    await hourKeyProcess({ key: key });
  });
}

(async () => {
  await updatePreviousKey();
})();


// Subscribe to 'article' channel
subscriber.subscribe('article', async (message,) => {
  await saveToCache(message);
});

// Save received message to cache
async function saveToCache(message) {
  if (await client.isOpen === false) { await client.connect(); }

  try {
    const article = JSON.parse(message);

    if (!article || typeof article !== 'object' || !article.id) {
      console.error('Invalid article object:', article);
      return;
    }
    await client.set(article.id + "-s", JSON.stringify(article), {
      EX: 5,
      NX: true,
    });
    await client.set(article.id + "-l", JSON.stringify(article), {
      EX: 3600,
      NX: true,
    });
    console.log('Saved caches success: ', article.id);
  } catch (e) {
    console.error(e);
  }
}

// Subscribe to key expiration events
// keyExpiredHandle.configSet('notify-keyspace-events', 'Ex', (err, res) => {
//   if (err) {
//     console.error('Error setting notify-keyspace-events:', err);
//   } else {
//     console.log('notify-keyspace-events set:', res);
//   }
// });

keyExpiredHandle.subscribe('__keyevent@0__:expired', (keyExpired, channel) => {
  handleExpiredKey({ key: keyExpired })
});

// Function to handle expired keys
async function handleExpiredKey({ key }) {
  if (await client.isOpen === false) { await client.connect(); }

  console.log(`Handling expired key: ${key}`);
  // Add your custom logic here
  try {
    const rmKey = key.slice(0, -1) + 'l';
    await hourKeyProcess({ key: rmKey });
  } catch (e) {
    console.error(e);
  }
}

async function hourKeyProcess({ key }) {
  if (await client.isOpen === false) { await client.connect(); }

  // Get data from cache - addDatabase if need
  console.log(`Data from hour cache: ${await client.get(key)}`);
  console.log(`Update database . . .`);

  // Remove key from redis
  try {
    await client.del(key);
    console.log(`Successfully Remove key: ${key}`);
  } catch (e) {
    console.error(e);
  }
}



const app = express();
// Middleware
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Server route
app.get('/article', async (req, res) => {
  if (await client.isOpen === false) { await client.connect(); }
  
  try {
    const article = {
      id: uuidv4(),
      name: 'Using Redis Pub/Sub with Node.js',
      blog: 'Logrocket Blog',
    };

    if (await client.get(article.id + "-l") || await client.get(article.id + "-s")) return res.json({ status: 666, message: 'Article Existed' });

    await publisher.publish('article', JSON.stringify(article));
    return res.json({ status: 200, message: `Publish Success:${JSON.stringify(article)}` })
  } catch (e) {
    console.error(e);
    return res.status(500);
  }
});
const port = 9736
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});


